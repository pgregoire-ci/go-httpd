# go-httpd

A go binary that serves the current directory over HTTP.


## Releases

Binaries are built on a weekly basis. The latest binary can be found at:

[https://gitlab.com/pgregoire-ci/go-httpd/-/jobs/artifacts/master/raw/httpd?job=build](https://gitlab.com/pgregoire-ci/go-httpd/-/jobs/artifacts/master/raw/httpd?job=build)
