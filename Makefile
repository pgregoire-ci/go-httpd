.POSIX:
.SUFFIXES: .go


BIN	:= httpd
SRC	:= ${BIN:=.go}


all: ${BIN}

.go:
	go build $<

clean:
	rm -f ${BIN}
