
package main

import (
	"flag"
	"log"
	"net/http"
)


var addr = flag.String("addr", "127.0.0.1:8000", "bind address")


func main() {
	flag.Parse();
	log.Fatal(http.ListenAndServe(*addr, http.FileServer(http.Dir("."))));
}
